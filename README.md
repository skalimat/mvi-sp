# Basket Generator
> Generating basket vectors and evaluating Neural Network architectures

## Assignment
This project is developed with dataset obtained by Recombee with their cooperation.
1. Analyze behavior of the customers in the Bofrost dataset. Are there patterns in the data? 
1. Write data generator that implements different shopping strategies and is parametric.
1. Compare the performance of basic and simple models on artificial data. 
Are they able to learn the simple behavior customers from the generated data?
How large does  the network need to be? What about it's overall architecture?

## Results
| name      | location                                                     |
|-----------|--------------------------------------------------------------|
| milestone | [docs/milestone/milestone.pdf](docs/milestone/milestone.pdf) |
| report    | [docs/report/report.pdf](docs/report/report.pdf)             |

## Data
Dataset used in this project is sadly not publicly available.

## How to run
To use the implemented generator class, check the `generator/` module. Folder `notebooks/` contains 
different notebooks that were used for development as well as evaluation. Usage is described in the script below.

```python
from genbasket.generator.batch import BatchGenerator

settings = {
    'history_size': 4,
    'basket_size': 128,
    'stride_min': 2,
    'stride_max': 3,
    'items_min': 2,
    'items_max': 8,
    'batch_size': 1024
}

generator = BatchGenerator.user_factory(
    users_count=65536, initial_seed=42, **settings
)
```

## Pushing to git
How to push to a fit branch on gitlab without the data. Ignores the commit history.
```bash
git checkout --orphan fit   
rm data/*.pickle data/*.csv
git add data/
git commit -m "All squashed commits without data"
git push -f -u fit
git checkout main
git branch -D fit
```