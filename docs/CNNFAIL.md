## Why do CNN's fail?

One would assume that by creating different 1D filters, the model would learn to simply predict patterns by applying different filters on the input. If some pattern is detected, then a filter gets activated and we predict a new value. Simple, right? Yet, let's assume the following completely predictable example:

```python
# Input data
X = [[0, 1, 1, 0],
     [0, 1, 0, 1],
     [0, 1, 1, 0],
     [0, 1, 0, 1]]

# Expected output
y =  [0, 1, 1, 0]
```

```python
# 3 filters for different behaviors
f = [[1, 0, 1],
     [1, 1, 0],
     [1, 0, 1],
     [1, 1, 0]]
```

```python
# Filters applied on the input
o = [[0, 4, 2, 2],
     [0, 0, 0, 2],
     [0, 0, 2, 0]]

# Sum pooled result
p = [[0, 4, 4, 4]]
````


Let's do that in code as a sanity check.

```python
x = np.array([[0, 1, 1, 0],
              [0, 1, 0, 1],
              [0, 1, 1, 0],
              [0, 1, 0, 1]], dtype="float32")
```


```python
from tensorflow.keras.layers import Conv1D
import numpy as np

conv = Conv1D(kernel_size=1, activation="relu", filters=3, data_format="channels_first")
conv.build(input_shape=(4, 4))

filters, bias = conv.get_weights()
filters[0][:, 0] = np.array([1, 1, 1, 1])
filters[0][:, 1] = np.array([0, 1, 0, 1])
filters[0][:, 2] = np.array([1, 0, 1, 0])
bias = np.array([0, 0, 0], dtype="float32")
conv.set_weights([filters, bias])
```

```text
<tf.Tensor: shape=(1, 3, 4), dtype=float32, numpy=
array([[[0., 4., 2., 2.],
       [0., 2., 0., 2.],
       [0., 2., 2., 0.]]], dtype=float32)>
```