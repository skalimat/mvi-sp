\documentclass[a4paper,10pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage[margin=1in]{geometry}
\usepackage{enumitem}

\setlist[itemize]{leftmargin=1.2in}


\usepackage[nottoc]{tocbibind}
\usepackage{fancyvrb} 
\usepackage{float}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{color}
\usepackage{booktabs}

% \cite{Yu2016ADR} 
% \cite{Rendle2010FactorizingPM} 


\title{Next Basket Prediction Data Generator\\NI-MVI Project Milestone}
\author{Matyáš Skalický\\skalimat@fit.cvut.cz}
\date{November 30, 2020}

\begin{document}
\maketitle

\tableofcontents

\medskip

% \begin{abstract}
% The goal of this is to create a synthetic data generator for the next-basket prediction task. First, we will explore the Bofrost dataset to get some information about a real life dataset. Based on that, we will construct a deterministic generator that generates baskets similar to the real data. We will then test different deep learning architectures and evaluate their results.
% \end{abstract}

\section*{Assignment}
This tasks relates to my summer project with Recombee. We have tried to implement a deep neural network architecture for the next-basket prediction task. This project is based on a proprietary Bofrost dataset which is sadly not publicable.
\begin{enumerate}
  \item Analyze behaviour of the customers in the Bofrost dataset. Are there patterns in the data?
  \item Write data generator that implements different shopping strategies and is parametric.
  \item Compare the performance of simple models on artificial data. Are they able to learn the simple behaviour customers from the generated data? How large does the network need to be? What about the overall architecture?
\end{enumerate}

\section{Introduction}
Bofrost is a company that offers a service where a loaded truck comes straight to your home with all the goods ready to sell. It is like a little shop on wheels that comes straight to next to your door. But this comes at a cost. Trucks need fuel and the storage space is limited. If, somehow, you could predict what the customer buys before you even meet him, you could dramatically lower the delivery costs as well as recommend better tailored products.

This task is usually dubbed as \emph{next-basket prediction}. We feed the model with the user's previous purchases. Based on this data, we try to predict the content of his next shopping basket. It turns out that due to the nature of the data as well as human behaviour, this task is really difficult to master.

First of all, purchases in our data were performed in the past. But the user's choice was influenced not only by his (random) choice, but also by the recommendation system that was in production at the time of purchase.  We are also influenced by sales, trends and general availability of the goods and seasonal events. We, humans, are curious creatures and we enjoy trying new things out.
 
Neural networks usually achieve state-of-the art results in tasks, where enough data is available. In case of Bofrost, we have over 19 million purchased baskets just from the year 2019 alone. Yet a trained deep neural network architecture had shown only a slight improvement over the naive models. A simple reminder model that recommends the previous purchase achieves almost comparable performance.

The question is why? Are the users behaving too randomly? Or are the selected architectures of the deep neural networks not powerful enough? What are the best architectures for this task? If we had a perfect world where users follow simple rules, would the neural networks work?

In this experiment, we simplify this task to the following: each item is in the input basket at most once. We also predict at most one item on the output. Another simplification is that we completely neglect the time dimension of the previous purchases. We model the users completely deterministic.

\section{Current Progress}
So far, I have implemented a very basic data generator. Most work was put into making all of the generators replicable if a seed is passed. We create a generator for each user. Another generator is then used to generate the data that can be fed into the neural network when training. The current naive user generator has several parameters:

\begin{itemize}
  \item[\emph{items count}] Minimum and maximum items in a single purchase
  \item[\emph{stride size}] How long until we repeat the previous purchase
\end{itemize}

The generator outputs sequences of previous purchases encoded as n-hot vectors as well as the target purchase vector from the generated sequence. I have also explored the dataset, the results are described below. So far, I have only experimented with training a simple LSTM-based architecture similar to the architecture used in DREAM \cite{Yu2016ADR} paper. Also, I have prepared functions for plotting and evaluating the results.

\section{State Of The Art}
There are not many papers regarding this exact subject. There is a paper called DREAM (A Dynamic Recurrent Model for Next Basket Recommendation) \cite{Yu2016ADR} from 2016 which using simple recurrent neural network predicts the next user's purchase and concludes that this is the current state of the art. Other approaches such as FPMC (Factorizing Personalized Markov Chains for Next-Basket Recommendation) \cite{Rendle2010FactorizingPM} take note of the fact, that the shopping decisions can be modelled by a Markov process.

\section{Data Exploration}
The dataset used in this experiment comes from 2019. It consists of 19,128,183 entries each denoting a single purchased basket. The dataset consists of 1265 unique item ids. As we can see in the Figure \ref{topk}, their popularity is not uniformly distributed. Top 800 most popular item ids capture over 99\% of all sold products. 

Figure \ref{basketsizes} vizualizes the sizes of typical baskets. We usually encode the shopping baskets as n-hot vectors of a dimension that is equal to the number of unique items in the datasets. If an item id is present in the dataset, we set the corresponding value of the n-hot vector to 1. We can conclude that such vectors will be pretty sparse.

Humans like to explore and buy new and original things. Figure \ref{nunique} illustrates how many unique items the user had purchased over time. Although the number grows, user still mostly purchase items they had purchased previously.

% Width of the smaller figures
\newcommand{\w}{0.9}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\w\linewidth]{images/topk.pdf}
  \caption{Share of top-k most popular items among all sold}
  \label{topk}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\w\linewidth]{images/basketsizes.pdf}
  \caption{Number of unique items in a single basket}
  \label{basketsizes}
\end{figure}


\begin{figure}[!htb]
  \centering
  \includegraphics[width=\w\linewidth]{images/nunique.pdf}
  \caption{Number of unique items an user had interacted with over time}
  \label{nunique}
\end{figure}

\clearpage
Figure \ref{purchases} contains example of purchases from the dataset. Note that these examples were generated from users that had interacted with 6 unique items and had exactly 10 purchases in the dataset. We can clearly see why a reminder approach works well. We can also note that some people really buy things repeatedly supporting our hypothesis that people are somewhat predictable.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.86\linewidth]{images/purchases.png}
  \caption{Example of purchases from the Bofrost dataset. Vertical axis captures the date of purchase. Color denotes number of purchased items. Ticks on the x axis describe the item id of the product.}
  \label{purchases}
\end{figure}

\section{Simple Experiments}
So far, I have only experimented with a very simple architecture consisting of a single LSTM layer and 2 dense layers. Although the users are completely predictable, I still haven't been able to achieve perfect accuracy either in training or validation data. I am not yet sure why is that. Some of the results is shown in the Figure \ref{examples}.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{images/example.png}
  \caption{Example outputs of a simple LSTM model}
  \label{examples}
\end{figure}

\section{Further Improvements}
I would like to compare the simple LSTM architecture with a GRU, CNN and also possibly attention layers in the future. For further experiments, I will probably need to create a parser to schedule multiple experiments on cloud. 

I also need to think more about the generator. Currently, there is no correlation between different items in the dataset. Yet in reality users often buy things together. Also, we can expect that there are similar users in the dataset. On the other hand, the task as-is right now is harder so if I manage to find an architecture that works well, it can only help.

I believe that this task could be related to predicting a cellular automata or Markov process of some sort. I have not spent enough time exploring literature regarding these topics.

\medskip
\bibliographystyle{unsrt}
\bibliography{sample.bib}

\end{document}