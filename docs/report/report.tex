\documentclass[a4paper,10pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage[margin=1in]{geometry}
\usepackage[shortlabels]{enumitem}


\setlist[itemize]{leftmargin=1.2in}


\usepackage[nottoc]{tocbibind}
\usepackage{fancyvrb} 
\usepackage{float}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{color}
\usepackage{listings}
\usepackage{booktabs}

\title{Next Basket Prediction Data Generator\\NI-MVI Project Report}
\author{Matyáš Skalický\\skalimat@fit.cvut.cz}
\date{December 25, 2020}

\begin{document}
\maketitle

\tableofcontents

\medskip

\section*{Assignment}
Task of this project relates to our research at Recombee, where we develop a deep neural network architecture for the next-basket prediction task. This project is based on a proprietary Bofrost dataset.

\begin{enumerate}
  \item Analyze behaviour of the customers in the Bofrost dataset. Are there patterns in the data?
  \item Write data generator that implements different shopping strategies and is parametric.
  \item Compare the performance of simple models on artificial data. Are they able to learn the simple behaviour customers from the generated data? How large does the network need to be? What about the overall architecture?
\end{enumerate}

\section{Introduction}
Bofrost\footnote{\url{https://www.bofrost.de/}} is a company that offers a service, where a loaded truck comes straight to your home with all the goods ready to sell. It is like a little shop on wheels that comes straight to your door. But this comes at a cost. Trucks need fuel and the storage space is limited. If, somehow, you could predict what the customer buys before you even meet him, you could dramatically lower the delivery costs as well as recommend better tailored products.

This task is usually dubbed as \emph{next-basket prediction}. We feed the model with the sequence of user's previous purchases. Based on this data, we try to predict the content of his next shopping basket. It turns out, that due to the nature of the data as well as the human behaviour, this task is really difficult to master. And this could be caused by one of many possible issues.

We, humans, are curious creatures and we enjoy trying new things out. We are influenced by sales, trends and general availability of the goods and seasonal events. Purchases in our training data were performed in the past. But the user choice was  based on only on her likings, randomness and previous purchases, but also on the recommendation system that was in production at the time of purchase. And we are training a new model on this data.

Neural networks usually achieve state-of-the art results in tasks, where enough data is available. In case of Bofrost, we have over tens of millions purchased baskets just from year 2019 alone. Yet a trained deep neural network architecture \cite{skalicky2020adm} had shown only a slight improvement over naive models. A reminder (that recommends the previous purchase) achieved almost comparable performance.

The question is why? Are the users behaving too randomly? Are the selected architectures of the neural networks not powerful enough? What are the best architectures for this task? If we had a perfect world, where users follow simple rules and limitless supply of data, would the neural networks work?

%nejak bych to cele lepe popsal - chci odpovedet na otazky vyse a tedy v ramci projektu analyzuji typicke bofrost uzivatele a vytvorim deterministicke zjednodusene virtualni uzivatele, jejichz chovani pak budu modelovat pomoci ruznych architektur ... tj. presne vim, co hledam, vytvorim patterny o rostouci slozitosti a zbenchmarkuji na techto ulohach jednotive architektury

To answer the questions above, we will represent the typical Bofrost users and create their synthetic counterparts. These generated users will then be benchmarked by different architectures while also increasing the complexity of users and patterns. In this project, we simplify this task to the following: each item is in the input basket at most once. We predict at most one item on the output. We completely neglect the time dimension of the previous purchases. We model the users as completely deterministic. 

\section{State Of The Art}
Traditional approach to the next-basket prediction was modelling the transactions as Markov chains \cite{Rendle2010FactorizingPM}. Other legacy approaches also utilised association rule mining (TARS \cite{guidotti2017next}). After 2016, deep learning methods became the new state of the art. LSTM seemed to be the way to go \cite{Yu2016ADR}, as we are predicting the next basket from the sequence of the previous baskets. Yet in later publications, several authors noticed that RNNs alone are not able to effectively model the next purchase. Usage of CNN was proposed as well \cite{tang2018personalized}. In recent years, we can see that attention based methods achieve the new state of the art.

\subsection{FPMC (Factorizing Personalized Markov Chains, 2010)}
Baseline approach is the \emph{Factorizing Personalized Markov Chains for Next-Basket Recommendation} \cite{Rendle2010FactorizingPM}. This work takes a note of the fact, that shopping decisions can be modelled by a Markov process. This method extracts sequential features from historical transactions and then predict the next purchases based on these sequential behaviours. 

\subsection{DREAM (A Dynamic Recurrent Model Next Basket Rec., 2016)}
A \emph{Dynamic Recurrent Model for Next Basket Recommendation} \cite{Yu2016ADR} first utilised deep learning for the next-basket prediction task. Individual baskets are encoded as sparse n-hot vectors. Such sequences of vectors are then passed into recurrent neural network (LSTM). DREAM establishes the new state of the art and introduced deep learning into next-basket prediction task.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{images/dream.png}
  \caption{A Dynamic Recurrent Model Next Basket Recommendation \cite{Yu2016ADR}.}
  \label{dream}
\end{figure}

%\subsection{TARS (Temporal Annotated Recurring Sequence, 2017)}
%Temporal Annotated Recurring Sequence uses association rule learning to achieve a new state of the art. It is noted, that current approaches are not capable to capture at the same time the difference factors influencing the customer's decision process: co-occurrency, sequentiality, periodicity and recurrency of the purchased items. Compared to the other models, TARS also takes in mind the timestamp of the purchase. This approach uses FP-Growth algorithm to build a FP-tree which captures the frequency at which itemsets occur in the dataset. 

\subsection{CASER (Convolutional Sequence Embedding Rec. Model, 2018)}
\emph{Convolutional Sequence Embedding Recommendation Model} \cite{tang2018personalized} aims to predict the top-n sequential items using convolutional filters. Filters are applied both on rows (basket) and columns (items) of precalculated basket embedding in a cross-shaped way. The vertical filters are of width 1 while the horizontal are of height 2. Results are pooled and concatenated into a final prediction with fully connected layers. Authors note that 'in sequential recommendation problem, not all adjacent actions have dependency relationships'. This can be the main limiting factor of RNN-based methods.

\subsection{AttRec (Self-Attentative Sequential Rec. Model, 2018)}
\emph{AttRec} \cite{zhang2018next} first introduce attention to the next-basket recommendation task. As the authors note, self-attention can replace RNN and CNN in sequence learning, achieving better accuracy with lower computation complexity. However, the usage of self-attention in the context of recommenders is far from straight-forward.

\begin{figure}[!htb]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/caser.png}
  \caption{Architecture of the CASER model \cite{tang2018personalized}.}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/attrec.png}
  \caption{AttRec self-attention module \cite{zhang2018next}.}
\end{subfigure}
\end{figure}


\subsection{IERT (Item Encoder Representations from Transformers, 2019)}
\emph{Item Encoder Representations from Transformers} \cite{yang2019pre} uses the proven BERT \cite{devlin2018bert} transformer architecture for the next-basket prediction task. IERT regards each item as a word in BERT, each basket as a sentence and each sequence of baskets of the same user as a document. Similar to BERT, learning procedure of IERT consists of the pre-training stage and fine-tuning stage. Pre-training stage was modified to produce context-aware item representations as well as to explore users' local transaction behaviours. Compared to BERT, among other differences, IERT does not optimise for the order of tokens (words) in pre-training as it deals with \emph{sets} of items. 

\subsection{Sets2Sets (Learning from Sequential Sets with Neural Networks, 2019)}
\emph{Sets2Sets} \cite{hu2019sets2sets} generalises the next-basket prediction as a prediction of a set of items (basket) from sequence of previous sets. Used approach is similar to seq2seq used in NLP – the model consists of an encoder and a decoder. Each set is first converted into a dense embedding, this embedding is then fed into a RNN unit. Set-based attention is used in between the encoder and decoder. Sets2Sets is more generalised than the next-basket prediction task as it allows to predict several sets (baskets) into the future.

\subsection{TIFU–KNN (Temporal Item Frequency based User KNN, 2020)}
It seems that 2020 is a year of predictive shopping. We can witness several new approaches to the next-basket prediction task. \emph{Temporal Item Frequency based User KNN} \cite{hu2020modeling} proposes usage of KNN to finding the most similar users. A big contribution of TIFU is the creation of a synthetic dataset. Authors try to show, that the RNN based methods are not suitable to learn the personalised item frequency (PIF). While the RNNs theoretically have the ability to learn, learning process sticks to a local minimum in current recommendation setting. Even with a KNN, authors note that this simple approach outperforms most of the deep-learning models.

\subsection{$M^2pht$, (Mixed Models with Preferences and Hybrid Transitions, 2020)}
\emph{Mixed Models with Preferences and Hybrid Transitions for Next-Basket Recommendation} \cite{peng2020m2pht} use three important factors in order to generate recommendations for each user: user's general preferences, the transition patterns among items across baskets and transition patterns among baskets. It uses GRU layers and also extracts information from the baskets using several learned matrices.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{images/m2pht.png}
  \caption{$M^2pht$ model structure \cite{peng2020m2pht}.}
  \label{m2pht}
\end{figure}

\section{Data generator}
Implemented synthetic data generator outputs sequences of previous purchases encoded as n-hot vectors as well as the target purchase vector from the generated sequence. Users are shuffled between batches.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.7\linewidth]{images/itemgen.png}
  \caption{Schematic representation of sliding window over baskets history in generator.}
  \label{itemgen}
\end{figure}


Current implementation of the data generator is naive in a sense, that it is:
\begin{enumerate}[(a), noitemsep]
	\item completely predictable,
	\item neglects the relationships between items,
	\item neglects similarities between users,
	\item neglects item exploration by users,
	\item ignores that items can be replaced by an alternative,
	\item does not generate metadata such as time of purchase.
\end{enumerate}

\clearpage
\subsection{Exploration of Real Data}
Data exploration presented in the milestone showed that, indeed, some users shop in patterns. Most of the shopping baskets also contain less than 5 products. We know that the users like to explore new products, but most of the purchased products are reoccurring purchases. This is one of the limitations of the current simple data generator, as it does not contain any relationships between the products or users. Yet if we know that we have a model that is able to learn the pattern on this synthetic task, we know that the model is capable of reproducing similar behaviour on the real data as well.

\begin{figure}[!htb]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/basketsizes.pdf}
  \caption{Number of unique items in a single basket}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/nunique.pdf}
  \caption{Unique items an user had interacted with over time}
\end{subfigure}
\end{figure}

Figure \ref{purchases} contains example of purchases from the Bofrost dataset. The examples were generated from users that had interacted with 6 unique items and had exactly 10 purchases in the dataset. We can clearly see why a reminder approach works well. We can note that some people really buy things repeatedly. This supports our hypothesis that people are somewhat predictable and that the model should be able to learn this pattern.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{images/purchases_old.png}
  \caption{Example of purchases from the Bofrost dataset. Vertical axis captures the date of purchase. Color denotes number of purchased items. Ticks on the x axis describe the item id of the product.}
  \label{purchases}
\end{figure}

\subsection{Generator Implementation}
The \lstinline{BatchGenerator} object inherits from the \lstinline{tensorflow.keras.utils.Sequence}. This allows for the generator to be easily used when training Tensorflow models even in parallel while training the model on GPU. Figure \ref{generatorsettings} shows the settings of presented generator.

\begin{figure}[!htb]
\begin{lstlisting}[language=Python]
from genbasket.generator import BatchGenerator

settings = {
    'history_size': 7,
    'basket_size': 128,
    'stride_min': 1,
    'stride_max': 5,
    'items_min': 2,
    'items_max': 8,
    'batch_size': 1024
}

generator = BatchGenerator.user_factory(
    users_count=32768, initial_seed=42, **settings
)
\end{lstlisting}
\caption{Settings of the BatchGenerator class}
\label{generatorsettings}
\end{figure}

\section{Models}
Goal of this project was to train several basic neural architectures to see, if they fail or succeed (given this simple and completely predictable problem and enough data). Go-to approach in this task is the LSTM over baskets encoded as n-hot vectors, Yet, as shown below, this architecture is not able to learn the shopping patterns of an item, that weren't in the training dataset. This means, that the model is not able to generalise between items. My research shows, that recurrent neural networks on baskets are not item-invariant and require a lot of training data to work well.

To simplify the task, user always buy at most 1 item each. We will optimise the \emph{categorical-crossentropy} between the last \emph{dense layer with sigmoid activations} and the ground truth. Our primary metric will be \emph{cosine similarity} as it is readily implemented in Tensorflow.

Our generator will be set with the following settings: it will be composed of 32 768 users with baskets of size 128. The model will see 7 steps in the past. User baskets contain 2 to 7 items each, stride (recurrence) is random between 1 and 4. All the models were trained for up to 128 epochs with default Adam optimiser. 

\subsection{Item CNN}
A natural way of detecting a pattern in purchases would be to use several 1D convolutional filters over the sequences of the items on the input and globally pool their activations. This is shown in the Figure \ref{itemcnn}. However, this approach had proven to be problematic and only able to predict the previous purchases.

I was quite surprised that the CNNs fail at this task so badly. My hypothesis explaining why CNNs fail, is due to the pooling mechanism. The pooling operation is not able to maintain context of the applied CNN filters. In the presented file, we can see that even if the CNN knew perfect filters, it still wouldn't be able to detect the periodicity of the purchases (thanks to the pooling mechanism). This is explained by example in file \emph{docs/cnnfail.md}.

Instead of pooling, we could use a single dense layer. Issue with this approach would be that the size of the model explodes if the basket size is too large.

\subsection{Basket LSTM}
Basket LSTM represents a simple RNN-based architecture (Figure \ref{basketlstm}) similar to DREAM \cite{Yu2016ADR}. We feed the model a sequence of n-hot vectors and it predicts the target n-hot vector on the output. LSTM does not return the hidden states in this case. Results of this model are discussed in the models comparison.

\subsection{Item-Distributed LSTM}
In order to fight the inability of LSTM network failing to generalise over patterns on different items, a model similar to 1D convolutional networks was created. Schematic visualisation is shown in the Figure \ref{distributedlstm}. Instead of convolutions, it uses several ($8$) small LSTM networks which are applied in item-distributed (time-distributed across purchases) manner. This could be probably reduced down to a single block of a right size. This model was first to completely master the problem on the synthetic data generator. 

Limitation of this approach is that each individual LSTM kernel is only able to see the history of only one item. Therefore it has no knowledge of the context which is important on the real data. This model also never predicts a new, novel item that the user hadn't purchased before.

\begin{figure}[!htb]
\centering
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/itemcnn.png}
  \caption{Item CNN, 256 filters}
  \label{itemcnn}
\end{subfigure}%
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/basketlstm.png}
  \caption{Basket LSTM}
  \label{basketlstm}
\end{subfigure}%
\begin{subfigure}[b]{.33\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/distributedlstm.png}
  \caption{Item-distributed LSTM}
  \label{distributedlstm}
\end{subfigure}
\caption{Simple architectures for the next-basket prediction task.}
\end{figure}

\subsection{Combined LSTM and CNN Architecture}
The problem of the time-distributed LSTM layers is that they have no information over the context of the other items (similar to the issue with pooling of CNNs). In order to fix this, we will use 1D convolutional filters distributed over baskets history (similar to CASER \cite{tang2018personalized}). This way, LSTM can also see the context and not only the current sequence. Architecture of this model is shown in Figure \ref{combinedlstm}. Presented also contains vertical CNN filters, which were not implemented in the evaluated model (dotted green).

Possible improvement is to this architecture is to include the item embedding (created by matrix factorisation) to the input of the LSTM (perhaps as the initial state). This way, each LSTM 'kernel' has further information about the current item its predicting. 

\begin{figure}[!htb]
\centering
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/combinedlstm.png}
  \caption{Item-distributed LSTM with CNN filter context}
  \label{combinedlstm}
\end{subfigure}%
\begin{subfigure}[b]{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/attentionlstm.png}
  \caption{LSTM with attention block}
  \label{attentionlstm}
\end{subfigure}
\caption{Other architectures for the next-basket prediction task.}
\end{figure}


\subsection{Attention-based LSTM architecture}
The attention-with-context model uses the attention layer \emph{AttentionWithContext} implementation from \emph{Hierarchical Attention Networks for Document Classification} \cite{yang2016hierarchical}. LSTM is used over sequence of previous baskets, however the hidden states are returned into attention layer as shown in Figure \ref{attentionlstm}. This way, attention mechanism is able to access the whole history of the RNN. 

\clearpage
\section{Comparison of implemented models}
Figure \ref{comparison} shows the validation accuracy of the implemented models over training epochs. 

LSTMs alone are not able to learn the periodicity of the user purchases as they are not item-invariant. They require to see the behaviour for given item in order to learn the pattern on the item\footnote{We could, perhaps, create a custom LSTM implementation that always sees the current input at the same place with the context of other purchases?}. Example of the LSTM model failing to capture the item-invariance can be seen in the Figure \ref{basketlstm_example}. While the item 27 was correctly predicted, item 40 (which shows the exactly same shopping pattern) was almost completely neglected.

\begin{figure}[!htb]
  \centering
  \includegraphics[height=6cm]{images/basketlstm_example.png}
  \caption{Example of trained basket LSTM model\\ failing to capture item-invariant sequences.}
  \label{basketlstm_example}
\end{figure}

Interestingly, LSTM with attention was much better than the baseline (simple) 'DREAM' model. This could be an interesting takeaway that could be very easily implemented in the production.

Distributed LSTM models \lstinline{lstm_distributed} and \lstinline{lstm_cnn_combined} models were able to master the patterns in the generated dataset. The benefit of the LSTM with CNN context was only negligible on this synthetic data. Yet on real data, there is a lot of noise and the context matters. It is questionable, whether these models would be able to learn anything except for simple reminder on real data.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\linewidth]{images/comparison.png}
  \caption{Comparison of the implemented models on synthetic dataset.}
  \label{comparison}
\end{figure}


\section{Conclusion}
Analysis of the dataset revealed, that while most of the users buy the previous items, there are also users that show evidence of periodicity in their item purchases. Based on the data analysis, a simple, deterministic synthetic data generator was implemented. Using the generator, several simple architectures were tested on a simplified task with completely predictable data, and enough training samples.

First interesting takeaway was that the 1D convolutions over the item purchases fail completely. I believe, that this is due to the pooling process neglecting the context of the other purchases. If the user purchases with periodicity of 2, the filter might detect this pattern and get activated. However after pooling, the network has no information which filter was activated and thus fails.

My prior approach to this task was using LSTM over the sequence of previous baskets. It came to me as a natural way of predicting the new basket from the sequence of previous purchases. Yet my research, as well as other materials\cite{hu2020modeling} suggest, that the RNNs lack the generalisation to learn shopping patterns over different items. This is likely due to the training process sticking into local minimum of predicting solely past purchase.

We know that LSTMs are not a perfect solution. I believe that there are multiple ways to tackle the problem more efficiently. One of such solutions would be to use the item-distributed LSTM filters (as presented above) but also include inputs from the row and item-wise CNN filters. This way, the predictor for each individual item knows the context of the previous purchases of the user on for each item as well as for each basket. On the other hand, it is questionable whether this approach is able to perform well even on real-world data.

Novel approach to this task is to utilise attention in some way. Current state-of-the-art language models widely use the transformer architecture. Compared to NLP, we do not care about the position of individual items in the basket. Therefore what we want to work with is sets. Solution could be as simple as not adding the positional embedding in the transformer architecture or take more inspiration from IERT \cite{yang2019pre}, which uses modified BERT transformer for the next basket prediction task.

A simple approach that showed some promise was the usage of attention on the output of the LSTM layer where we also return the hidden states. A nice side effect of attention would be the ability to see, which items were 'activated' in order to predict given purchases and possibly adjust the models. Attention could be an improvement, but will it solve the problem with item invariance? I am not sure. 

My overall hypothesis is that shopping really is a Markov process. User purchases are mostly influenced by the last, let's say 5 purchases. A benefit of deep learning method should be the ability to predict items based on the inter-user and inter-item similarity. While training, it is understandable that the model learns to predict the reminder as the loss function is generally minimised at that point. Creating a better suiting loss function that supports novelty could be an interesting field of research.

%\medskip
\clearpage
\bibliographystyle{unsrt}
\bibliography{references.bib}

\end{document}