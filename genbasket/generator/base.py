from abc import ABC
import numpy as np
from numpy.random import RandomState
import random


class GeneratorBase(ABC):
    def __init__(self, initial_seed=None):
        """Initialize the generator with a given initial seed"""
        self.base_seed = self.rehash(initial_seed) if initial_seed is None else random.randint(0, 2 ** 31)
        self.__internal_index = -1

        self.random_generator = RandomState(seed=self.base_seed)

    @staticmethod
    def rehash(x):
        return abs(hash(x)) % (2 ** 31)

    def set_state(self, state):
        """Set the random seed of a generator with respect to the given state"""
        self.random_generator.seed(self.base_seed + state)

    def internal_index(self, stride_length):
        """Get and update the internal index counter of a generator"""
        self.__internal_index = self.__internal_index + 1
        return self.__internal_index % stride_length

    def __getitem__(self, index: int) -> np.array:
        raise NotImplementedError
