from genbasket.generator.user import UserGenerator
from genbasket.generator.base import GeneratorBase
from tensorflow.keras.utils import Sequence
import numpy as np


class BatchGenerator(GeneratorBase, Sequence):
    def __init__(self, history_size, batch_size, initial_seed=None, generators=None):
        """Generates a single batch of specified """
        super().__init__(initial_seed=initial_seed)

        self.history_size = history_size
        self.batch_size = batch_size
        self.generators = np.array(generators or [])

        # When setting the generators externally indexes must be updated
        self.indexes = np.arange(len(self.generators))

    def on_epoch_end(self):
        self.random_generator.shuffle(self.indexes)

    def __len__(self):
        return int(np.floor(len(self.generators) / self.batch_size))

    def __getitem__(self, index: int) -> np.array:
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]
        selected = np.take(self.generators, indexes)

        X = np.array([gen.range(index, index + self.history_size) for gen in selected])
        y = np.array([gen[index+self.history_size] for gen in selected])

        return X, y

    @classmethod
    def user_factory(
        cls, history_size, batch_size, users_count, basket_size, stride_min, stride_max, items_min, items_max, initial_seed
    ):
        """Init the class with individual user generators"""
        batch_generator = cls(history_size, batch_size, initial_seed)
        generators = [
            UserGenerator(
                basket_size=basket_size,
                stride_length=batch_generator.random_generator.randint(stride_min, stride_max),
                items_min=items_min,
                items_max=items_max,
                initial_seed=batch_generator.base_seed + i
            )
            for i in range(users_count)
        ]

        batch_generator.generators = generators
        batch_generator.indexes = np.arange(len(generators))

        return batch_generator
