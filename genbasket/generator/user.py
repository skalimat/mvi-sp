import numpy as np
from .base import GeneratorBase


class UserGenerator(GeneratorBase):
    """Generates multiple baskets of an user"""
    def __init__(self, basket_size=10, stride_length=2, items_min=1, items_max=5, initial_seed=None):
        """Generator that generates purchases of a single user"""
        super().__init__(initial_seed=initial_seed)

        self.stride_length = stride_length
        self.basket_size = basket_size
        self.min_items = items_min
        self.max_items = items_max

    def __getitem__(self, index: int) -> np.array:
        # Set the random seed
        self.set_state(index % self.stride_length)

        # Generate the one-hot embeddings
        num_items = self.random_generator.randint(self.min_items, self.max_items + 1)
        item_onehots = np.eye(self.basket_size)
        item_picks = self.random_generator.choice(self.basket_size, num_items, replace=False)

        return np.sum(item_onehots[item_picks], axis=0)

    def range(self, start_index, end_index):
        return np.array([self[i] for i in range(start_index, end_index)])

    def __next__(self):
        return self[self.internal_index(self.stride_length)]

