from tensorflow.keras.losses import cosine_similarity
import tensorflow as tf


def mse_autorec_loss(y_actual, y_pred):
    a = tf.keras.backend.pow(tf.keras.activations.relu(y_actual, max_value=1) * y_pred - y_actual, 2)
    return tf.constant(0.2) * tf.math.divide(
        tf.keras.backend.sum(a),
        tf.keras.backend.cast(tf.math.count_nonzero(a), tf.keras.backend.dtype(a)),
    )


def cosmse(x,y):
    a = tf.constant(1.0)*(cosine_similarity(x,y) + tf.constant(1.0))
    d = tf.constant(0.2)*mse_autorec_loss(x, y)
    return a+d


def positive_cos_loss(x, y):
    return tf.keras.losses.cosine_similarity(x, y) + tf.constant(1.0)
