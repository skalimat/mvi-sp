import numpy as np


def nhot_to_ids(vector):
    return [i for i, value in enumerate(vector) if value > 0]


def nhot_from_itemids(all_itemids, row_itemids):
    assert set(row_itemids).issubset(all_itemids),\
        "row contains unknown itemids"
    row_indexes = [np.where(all_itemids == itemid)[0][0] for itemid in row_itemids]
    return np.eye(len(all_itemids)).take(row_indexes, axis=0).sum(axis=0)
