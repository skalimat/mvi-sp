import matplotlib.pyplot as plt
import numpy as np


def plot_predictions(x, y_true, y_raw):
    """Plot the predictions based on the X, y_true and raw y_pred matrices"""
    y_pred = np.round(y_raw)

    # Create a matrix with nonzero item ids
    purchase_matrix = np.vstack([x, y_true, y_pred])
    unique_itemids = np.nonzero(purchase_matrix.sum(axis=0))[0]

    # Remove all zero columns in the purchase_matrix
    idx = np.argwhere(np.all(purchase_matrix[..., :] == 0, axis=0))
    pruned_matrix = np.delete(purchase_matrix, idx, axis=1)
    pruned_raw = np.delete(y_raw, idx)

    X_matrix = pruned_matrix[:-2]
    y_true_matrix = pruned_matrix[-2:-1]
    y_pred_matrix = np.vstack([pruned_matrix[-1:], pruned_raw])

    # Actual plot displays 2 subplots
    gridspec_kw = {'height_ratios': [X_matrix.shape[0], y_true_matrix.shape[0], y_pred_matrix.shape[0]]}
    fig, ax = plt.subplots(nrows=3, gridspec_kw=gridspec_kw)
    ax[0].matshow(X_matrix, vmin=0, vmax=1)
    ax[0].set_xticks(range(len(unique_itemids)))
    ax[0].set_xticklabels(unique_itemids, fontsize=12, rotation=90)
    ax[0].tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

    ax[1].matshow(y_true_matrix, vmin=0, vmax=1)
    ax[1].set_xticks([])
    ax[1].set_yticks([0])
    ax[1].set_yticklabels(["true"])

    ax[2].matshow(y_pred_matrix, vmin=0, vmax=1)
    ax[2].set_xticks([])
    ax[2].set_yticks([0, 1])
    ax[2].set_yticklabels(["rounded", "rawpred"])

    plt.show()
